import { IBooking } from "../interfaces/ibooking.interface";
import  * as _ from 'lodash'; 

export class Booking implements IBooking {

    constructor(data : any){
        _.set(this, 'data', data);
    }

    get name(): any{
        return _.get(this, 'data.name');
    }

    get date(): any{
        return _.get(this, 'data.date');
    }

    get service(): any{
        return _.get(this, 'data.service');
    }
    
    getData(){
        return _.get(this, 'data');
    }
}
