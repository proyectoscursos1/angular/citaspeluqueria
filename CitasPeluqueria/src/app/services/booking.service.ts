import { IBooking } from './../interfaces/ibooking.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Booking } from '../models/booking';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }

  getBookings(): Observable<IBooking[]> {
    var headers = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json');

    const url = 'https://booking-app-9037d-default-rtdb.firebaseio.com/bookings.json';

    return this.http.get<IBooking[]>(url, { headers: headers }).pipe(map(data => {
      let bookings: any[] = [];
      if (data) {
        console.log(data);
        const today = new Date();
        _.forEach(_.keys(data), (key: any) => {
          const booking = new Booking(data[key]);
          const bookingDate = new Date(booking.date);
          if (bookingDate.getTime() >= today.getTime()) {
            bookings.push(booking);
          }
        });
      }
      bookings = _.orderBy(bookings, b => b.date);
      return bookings;
    }))
  }

  addBooking(booking: Booking) {

    var headers = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json');

    const url = 'https://booking-app-9037d-default-rtdb.firebaseio.com/bookings.json';

    const body = JSON.stringify(booking.getData());

    return this.http.post(url, body, { headers: headers })

  }

}
