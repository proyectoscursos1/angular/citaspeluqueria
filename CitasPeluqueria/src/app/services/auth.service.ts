import { Observable, map } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import  * as _ from 'lodash'; 

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }


/* Pendiente crear la interfaz */
  login(infoLogin: any): Observable <boolean> {

    var headers = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json');
  
    const url = 'https://booking-app-9037d-default-rtdb.firebaseio.com/users.json';

    return this.http.get(url, { headers : headers}).pipe(map(users => {

      const user = _.find(users, (u:any) => u.user === infoLogin.user && u.pass ==infoLogin.pass);
      
      if(user){
        return true;
      }
      return false;
    }));
  }


  isAutenticated(){
    return localStorage.getItem("logged");
  }
}
