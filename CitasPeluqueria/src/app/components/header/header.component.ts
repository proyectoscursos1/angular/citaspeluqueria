import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { LoginComponent } from './login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private modalService: NgbModal,
              private router: Router,
              public authService: AuthService) { }

  ngOnInit() {
  }

  /* Agregar el componente login dinamicamente */
  openLogin(){
    this.modalService.open(LoginComponent)
  }

  logout(){
    localStorage.removeItem("logged");
    this.router.navigate(['/add-booking']);
  }

}
