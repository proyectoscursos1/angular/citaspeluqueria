import { AuthService } from './../../../services/auth.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public formLogin: FormGroup;
  public showLoginSuccess: boolean;
  public showLoginError: boolean;

  constructor(public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private authService: AuthService, 
    private router: Router) {

    this.formLogin = this.formBuilder.group({
      user: new FormControl('', Validators.required),
      pass: new FormControl('', Validators.required)
    });

    this.showLoginSuccess = false;
    this.showLoginError = false;

  }

  ngOnInit() {
  }

  ckeckLogin() {
    this.showLoginSuccess = false;
    this.showLoginError = false;

    this.authService.login(this.formLogin.value).subscribe(success => {
      console.log(success);
      if (success) {
        this.showLoginSuccess = true;
        /* Reedireccionar a otra ruta */
        this.router.navigate(['/list-bookings']);
        localStorage.setItem("logged", "1");
        this.activeModal.close();
      } else {
        this.showLoginError = true;
      }

    });
  }


}
