import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { BookingService } from './../../services/booking.service';
import { Component, OnInit } from '@angular/core';
import { IBooking } from 'src/app/interfaces/ibooking.interface';

@Component({
  selector: 'app-list-bookings',
  templateUrl: './list-bookings.component.html',
  styleUrls: ['./list-bookings.component.css']
})
export class ListBookingsComponent implements OnInit {

  public listBookings: IBooking[];
  public loadBookings: boolean;

  constructor(private bookingService: BookingService,
    private authservice: AuthService,
    private router: Router) {

    this.listBookings = [];
    this.loadBookings = false;
  }

  ngOnInit() {

    if (this.authservice.isAutenticated()) {

      this.bookingService.getBookings().subscribe(list => {
        console.log(list);
        this.listBookings = list;
        this.loadBookings = true;
      }, error => {
        console.error("No se han podido recuperar los bookings: " + error);
        this.loadBookings = true;

      });

    } else {
      this.router.navigate(['/add-booking']);
    }


  }
}